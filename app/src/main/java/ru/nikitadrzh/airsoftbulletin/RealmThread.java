package ru.nikitadrzh.airsoftbulletin;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Nekit on 22.02.2018.
 */

public class RealmThread<T> extends HandlerThread {//T - элемент UI для обновления после загрузки
    private static final String TAG = "RealmThread";//название для конструктора
    private static final int MESSAGE_DOWNLOAD = 0;//для message

    private boolean mHasQuit = false;
    private StaticHandler requestHandler;//handler thread
    private Handler responseHandler;//handler UI
    private ConcurrentMap<T, String> requestMap = new ConcurrentHashMap<>();//тут храним url адреса
    private RealmResults<FavouritesItem> results;
    private RealmResultListener<T> realmResultListener;

    public interface RealmResultListener<T> {
        void onRealmResultObtained(T target, String results);//метод реализованный в активити
    }

    public void setOnRealmResultListener(RealmResultListener<T> listener) {
        realmResultListener = listener;
    }

    public RealmThread(Handler uiHandler) {//сюда передается ui handler
        super(TAG);
        responseHandler = uiHandler;
    }

    public void queueItems(T target, String url) {
        if (url == null) {
            requestMap.remove(target);
        } else {
            requestMap.put(target, url);//хранение url
            requestHandler.obtainMessage(MESSAGE_DOWNLOAD, target).sendToTarget();//получаем
            // сообщение, связанное с хендлером и отправляем ему
        }
    }

    @Override
    protected void onLooperPrepared() {
        requestHandler = new StaticHandler(this);//хоть статик, но имеет weak ссылку на
        // класс
    }

    private static class StaticHandler<T> extends Handler {
        private final WeakReference<RealmThread> realmThreadWeakReference;

        private StaticHandler(RealmThread realmThread) {
            realmThreadWeakReference = new WeakReference<>(realmThread);
        }

        @Override
        @SuppressWarnings("unchecked")
        public void handleMessage(Message msg) {

            if (msg.what == MESSAGE_DOWNLOAD) {
                T target = (T) msg.obj;
                RealmThread realmThread = realmThreadWeakReference.get();
                realmThread.handleRequest(target);
            }
        }
    }

    private void handleRequest(final T target) {
        Realm realm = Realm.getDefaultInstance();
        final String url = requestMap.get(target);//с этой ссылкой ищем в базе объявления
        final String resultString;

        if (url == null) {
            return;
        }
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    results = realm //находим элементы в базе с одним и тем же фото
                            .where(FavouritesItem.class)
                            .equalTo("itemPhotoUrl", url)
                            .findAll();
                }
            });
        } finally {
            resultString = results.toString();
            realm.close();
        }
        responseHandler.post(new Runnable() {//теперь тут создается сообщение для ui handler
            // используя interface
            @Override
            public void run() {
                if (!requestMap.get(target).equals(url) || mHasQuit) {//нужно из-за recyclerView
                    return;
                }
                requestMap.remove(target);//удаляется связь холдер с url, тк recyclerView
                realmResultListener.onRealmResultObtained(target, resultString);//вызываем
                // метод слушателя(он в активити)
            }
        });
    }

    @Override
    public boolean quit() {//метод завершения потока, вызывается в onDestroy
        mHasQuit = true;
        return super.quit();
    }

    public void clearQueue() {//очистка очереди
        requestHandler.removeMessages(MESSAGE_DOWNLOAD);
        requestMap.clear();
    }
}
