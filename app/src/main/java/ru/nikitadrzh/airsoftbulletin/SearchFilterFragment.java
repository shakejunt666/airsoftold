package ru.nikitadrzh.airsoftbulletin;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import io.realm.Realm;
import io.realm.RealmResults;

import static javax.xml.transform.OutputKeys.METHOD;

/**
 * Created by Nekit on 16.02.2018.
 */

public class SearchFilterFragment extends Fragment {
    private Spinner category;
    private EditText search;
    private EditText seller;
    private FloatingActionButton foundButton;
    private Callbacks callbacks;

    public static SearchFilterFragment newInstance() {//статический метод создания фрагмента
        Bundle args = new Bundle();
        args.putString(METHOD, "getAllItemsList");
        SearchFilterFragment fragment = new SearchFilterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_filter, container, false);
        category = view.findViewById(R.id.category_spinner);
        search = view.findViewById(R.id.search_edit_text);
        seller = view.findViewById(R.id.seller_id_edit_text);
        foundButton = view.findViewById(R.id.floating_search_button);
        foundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int filterSeller;
                if (seller.getText().toString().equals("")) {//чтоб небыло nullpointer
                    filterSeller = 0;
                } else {
                    filterSeller = Integer.parseInt(seller.getText().toString());
                }
                callbacks.onSearch(getSpinnerItem(category.getSelectedItem().toString()),
                        search.getText().toString(),
                        filterSeller);
            }
        });
        return view;
    }

    public interface Callbacks {//интерфейс обратного вызова, реализуемый активити-хостом

        void onSearch(String category, String search, int seller);//метод поиска, который будет
        // вызываться из активити
    }

    private String getSpinnerItem(String item) {
        switch (item) {
            case "Все категории":
                return "";
            case "Привода":
                return "BLASTERS";
            case "Экипировка и снаряжение":
                return "GEAR";
            case "Аксессуары и запчасти":
                return "PARTS";
            case "Смешанные альбомы":
                return "MIXED";
            default:
                return "";
        }
    }

    @Override
    public void onAttach(Context context) {//назначается активити
        super.onAttach(context);
        callbacks = (Callbacks) context;//активити преобразуется к callback, callback определяет
        // context из которого будет реализовываться интерфейс Callback
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }
}
