package ru.nikitadrzh.airsoftbulletin;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Cache;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

public class AirsoftActivity extends SingleFragmentActivity
        implements SearchFilterFragment.Callbacks {
    private static final String VK_APP_PACKAGE_ID = "com.vkontakte.android";
    private Fragment returnedFragment;
    public static Fragment staticFragment;

    @Override
    protected Fragment createFragment(String fragment) {//метод вызывается из родителя и создает
        // экз. фраг.
        switch (fragment) {
            case "ads":
                returnedFragment = AirsoftFragment.newInstance("getAllItemsList",
                        null, null, 0);
                staticFragment = returnedFragment;
                break;
            case "search":
                returnedFragment = SearchFilterFragment.newInstance();
                break;
            case "favorites":
                returnedFragment = FavouritesFragment.newInstance();
                break;
        }
        return returnedFragment;
    }

    @Override
    public void onSearch(String category, String search, int seller) {//метод обратного вызова
        // фрагмента Search
        returnedFragment = AirsoftFragment.newInstance("getSortedItemsList", category,
                search, seller);
        setFragment(returnedFragment);
        getBottomView().setSelectedItemId(R.id.navigation_ads);
    }

    public void startConversation(String url) {//метод, который открывает диалог с продавцом в ВК
        //TODO нужно сделать, чтобы в диалог лот отправлялся, тут баг, если из приложения выйти, то
        //TODO авторизацию не предлагает, в вебе все ок
        Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        List<ResolveInfo> resInfo = this.getPackageManager()//список с активити,
                // обрабатывающих такой интент
                .queryIntentActivities(sendIntent, 0);

        for (ResolveInfo info : resInfo) {
            if (VK_APP_PACKAGE_ID.equals(info.activityInfo.packageName)) {//если есть приложение VK,
                // то открываем в нем, иначе открывается в браузере
                sendIntent.setPackage(VK_APP_PACKAGE_ID);
            }
        }

        if (sendIntent.resolveActivity(getApplicationContext().getPackageManager()) != null) {//тут
            // проверка наличия приложения обрабатывающего такой интент
            startActivity(sendIntent);
        }
    }

    public void getFullAdImage(String itemPhotoUrl) {
        LayoutInflater inflater = this.getLayoutInflater();
        View alertView = inflater.inflate(R.layout.full_photo_view, null);
        ImageView fullPhotoView = alertView.findViewById(R.id.full_photo_image_view);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(alertView)
                .create();
        dialog.show();
        Picasso.with(this)//добавляется фотография объявления
                .load(itemPhotoUrl)
                .transform(new Transformation() {
                    @Override
                    public Bitmap transform(Bitmap source) {
                        int maxSize = 2048;
                        int size = Math.max(source.getWidth(), source.getHeight());
                        if (size > maxSize) {
                            int difference = size - maxSize;
                            int x = source.getWidth() - difference;
                            int y = source.getHeight() - difference;
                            Bitmap result = Bitmap
                                    .createScaledBitmap(source, x, y, false);
                            source.recycle();
                            return result;
                        } else {
                            return source;
                        }
                    }

                    @Override
                    public String key() {
                        return "resized photo";
                    }
                })
                .into(fullPhotoView);
    }

    public LruCache initPhotoCache() {//метод создания кэша для предварительной загрузки
        return new LruCache(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        staticFragment = null;
    }
}

