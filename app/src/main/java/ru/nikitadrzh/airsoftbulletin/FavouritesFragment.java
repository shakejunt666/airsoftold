package ru.nikitadrzh.airsoftbulletin;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.HashSet;
import java.util.Set;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

import static javax.xml.transform.OutputKeys.METHOD;

/**
 * Created by Nekit on 23.02.2018.
 */

public class FavouritesFragment extends Fragment {
    private static final String TAG = "FavouritesFragment";

    private Realm realm;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private FavouritesRecyclerViewAdapter adapter;
    private int richMediaWigth;//ширина контента cardView
    private int richMediaHeight;

    public static FavouritesFragment newInstance() {
        //статический метод создания фрагмента
        Bundle args = new Bundle();
        args.putString(METHOD, "getAllItemsList");//строка для проверки в AirsoftFragment
        FavouritesFragment favouritesFragment = new FavouritesFragment();
        favouritesFragment.setArguments(args);
        return favouritesFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getRichMediaSize();//ширина, высота контента cardView
        realm = Realm.getDefaultInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourites, container, false);
        recyclerView = view.findViewById(R.id.recycler_view_favourites);
        progressBar = view.findViewById(R.id.progress_bar_favourites);
        progressBar.setVisibility(View.VISIBLE);//TODO потом раскомментить
        setUpRecyclerView();
        return view;
    }

    private void setUpRecyclerView() {
        adapter = new FavouritesRecyclerViewAdapter(realm.where(FavouritesItem.class).findAll(),
                true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);

        TouchHelperCallback touchHelperCallback = new TouchHelperCallback();
        ItemTouchHelper touchHelper = new ItemTouchHelper(touchHelperCallback);
        touchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        recyclerView.setAdapter(null);
        realm.close();
    }

    private void getRichMediaSize() {//возвращается размер фотографии объявления
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();//контейнер, содержащий 2 координаты
        display.getSize(size);
        double widthOfScreen = size.x - 16;//16 - учет отступа cardView от края экрана

        richMediaWigth = (int) widthOfScreen;//ширина
        richMediaHeight = (int) (widthOfScreen / 16 * 9);//высота
    }

    /*
    Далее внутренний класс адаптера
     */
    private class FavouritesRecyclerViewAdapter extends RealmRecyclerViewAdapter<FavouritesItem,
            FavouritesRecyclerViewAdapter.FavouritesViewHolder> {
        private boolean inDeletionMode = false;
        private Set<Integer> countersToDelete = new HashSet<Integer>();

        public FavouritesRecyclerViewAdapter(@Nullable OrderedRealmCollection<FavouritesItem> data,
                                             boolean autoUpdate) {
            super(data, autoUpdate);
        }

        void enableDeletionMode(boolean enabled) {//TODO как юзать???
            inDeletionMode = enabled;
            if (!enabled) {
                countersToDelete.clear();
            }
            notifyDataSetChanged();
        }

        Set<Integer> getCountersToDelete() {//TODO как юзать???
            return countersToDelete;
        }

        @Override
        public FavouritesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getActivity())
                    .inflate(R.layout.ad_item, parent, false);
            return new FavouritesViewHolder(view);
        }

        @Override
        public void onBindViewHolder(FavouritesViewHolder holder, int position) {
            FavouritesItem obj = getItem(position);//получаем объект и передаем его в holder
            holder.data = obj;
            if (obj != null) {
                holder.descriptionTextView.setText(obj.getItemDescription());
                holder.descriptionTextView.setVisibility(View.GONE);
                holder.dateTextView.setText(obj.getItemAddTime());
                holder.sellerNameView.setText(obj.getSellerName());
                holder.cityNameView.setText(obj.getSellerCity());
                holder.marketNameView.setText(obj.getMarketName());
                holder.favouriteButton.setImageResource(R.drawable.ic_star_colored);
                holder.expandButton.setImageResource(R.drawable.ic_expand_more);
                holder.sellerVkId = obj.getSellerVkId();
                holder.itemPhotoUrl = obj.getItemPhotoUrl();

                Picasso.with(getActivity())//добавляется фотография объявления
                        .load(obj.getItemPhotoUrl())
                        .resize(richMediaWigth, richMediaHeight)//ширина, высота под cardView//TODO сделать по-другому
                        .centerCrop()//TODO обрезаем под resize, сохраняя соотношения сторон, в
                        //TODO идеале, нужно с сервера сразу маленький размер брать, пока так
                        .into(holder.adItemImageView);
                Picasso.with(getActivity())//добавляется фотография продавца, тут resize не нужен, тк
                        // аватарки ВК всего 200px в диаметре
                        .load(obj.getSellerPhotoUrl())
                        .into(holder.sellerImageView);
                //TODO сюда код с удалением вставить!!!
            }
        }

        class FavouritesViewHolder extends RecyclerView.ViewHolder {
            public FavouritesItem data;//сюда передается объект из binder
            TextView descriptionTextView;
            TextView dateTextView;
            TextView sellerNameView;
            TextView cityNameView;
            TextView marketNameView;
            ImageView adItemImageView;
            ImageView sellerImageView;
            ImageButton expandButton;
            ImageButton favouriteButton;

            String sellerVkId;
            String itemPhotoUrl;

            FavouritesViewHolder(View itemView) {
                super(itemView);
                descriptionTextView = itemView.findViewById(R.id.description_text_view);
                adItemImageView = itemView.findViewById(R.id.ad_item_image_view);
                adItemImageView.setOnClickListener(v -> ((AirsoftActivity) getActivity())
                        .getFullAdImage(itemPhotoUrl));

                sellerNameView = itemView.findViewById(R.id.seller_name_text_view);
                sellerImageView = itemView.findViewById(R.id.seller_image_view);
                sellerImageView.setOnClickListener(v -> ((AirsoftActivity) getActivity())
                        .startConversation("https://vk.com/im?sel=" + sellerVkId));

                cityNameView = itemView.findViewById(R.id.city_name_text_view);
                marketNameView = itemView.findViewById(R.id.market_name_text_view);
                dateTextView = itemView.findViewById(R.id.date_text_view);

                expandButton = itemView.findViewById(R.id.expand_button);
                expandButton.setOnClickListener(v -> changeDescriptionVisibility());
                favouriteButton = itemView.findViewById(R.id.button_favourite);
            }

            public void setData(FavouritesItem data) {
                this.data = data;
            }

            private void changeDescriptionVisibility() {//метод изменения видимости описания товара
                if (descriptionTextView.getVisibility() == View.GONE) {
                    descriptionTextView.setVisibility(View.VISIBLE);
                    expandButton.setImageResource(R.drawable.ic_expand_less);
                } else {
                    descriptionTextView.setVisibility(View.GONE);
                    expandButton.setImageResource(R.drawable.ic_expand_more);
                }
            }
        }
    }

    /*TouchCallback*/
    private class TouchHelperCallback extends ItemTouchHelper.SimpleCallback {
        TouchHelperCallback() {
            super(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT
                    | ItemTouchHelper.RIGHT);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                              RecyclerView.ViewHolder target) {
            Log.i(TAG, "moved");
            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            deleteSwipedItem(viewHolder);
        }

        private void deleteSwipedItem(RecyclerView.ViewHolder viewHolder) {
            RealmResults<FavouritesItem> results = realm.where(FavouritesItem.class).findAll();
            FavouritesItem item = results.get(viewHolder.getAdapterPosition());//позиция в базе и в
            // адаптере совпадает, потому тут сделано так
            realm.executeTransaction(realm1 -> {
                if (item != null) item.deleteFromRealm();//удаляем из базы запись
            });
        }
    }
}
