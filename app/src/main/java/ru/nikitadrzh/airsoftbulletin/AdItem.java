package ru.nikitadrzh.airsoftbulletin;

import java.util.List;

import io.realm.RealmObject;

/**
 * Created by Nekit on 08.02.2018.
 */

public class AdItem {//внутренний класс, представляет одно объявление

    private List<Content> content = null;
    private Pageable pageable;
    private Boolean last;
    private Integer totalPages;
    private Integer totalElements;
    private Boolean first;
    private Sort_ sort;
    private Integer numberOfElements;
    private Integer size;
    private Integer number;

    public List<Content> getContent() {
        return content;
    }

    public void setContent(List<Content> content) {
        this.content = content;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }

    public Boolean getLast() {
        return last;
    }

    public void setLast(Boolean last) {
        this.last = last;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }

    public Boolean getFirst() {
        return first;
    }

    public void setFirst(Boolean first) {
        this.first = first;
    }

    public Sort_ getSort() {
        return sort;
    }

    public void setSort(Sort_ sort) {
        this.sort = sort;
    }

    public Integer getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(Integer numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public class Content {

        private Integer id;
        private Seller seller;
        private VkAlbum vkAlbum;
        private String category;
        private Integer photoId;
        private String url;
        private String photoSrc;
        private String info;
        private Integer timestamp;
        private VkGroup vkGroup;
        private List<Object> duplicates = null;
        private Boolean deleted;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Seller getSeller() {
            return seller;
        }

        public void setSeller(Seller seller) {
            this.seller = seller;
        }

        public VkAlbum getVkAlbum() {
            return vkAlbum;
        }

        public void setVkAlbum(VkAlbum vkAlbum) {
            this.vkAlbum = vkAlbum;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public Integer getPhotoId() {
            return photoId;
        }

        public void setPhotoId(Integer photoId) {
            this.photoId = photoId;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getPhotoSrc() {
            return photoSrc;
        }

        public void setPhotoSrc(String photoSrc) {
            this.photoSrc = photoSrc;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }

        public Integer getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Integer timestamp) {
            this.timestamp = timestamp;
        }

        public VkGroup getVkGroup() {
            return vkGroup;
        }

        public void setVkGroup(VkGroup vkGroup) {
            this.vkGroup = vkGroup;
        }

        public List<Object> getDuplicates() {
            return duplicates;
        }

        public void setDuplicates(List<Object> duplicates) {
            this.duplicates = duplicates;
        }

        public Boolean getDeleted() {
            return deleted;
        }

        public void setDeleted(Boolean deleted) {
            this.deleted = deleted;
        }

    }

    public class Pageable {

        private Sort sort;
        private Integer pageSize;
        private Integer pageNumber;
        private Integer offset;
        private Boolean paged;
        private Boolean unpaged;

        public Sort getSort() {
            return sort;
        }

        public void setSort(Sort sort) {
            this.sort = sort;
        }

        public Integer getPageSize() {
            return pageSize;
        }

        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }

        public Integer getPageNumber() {
            return pageNumber;
        }

        public void setPageNumber(Integer pageNumber) {
            this.pageNumber = pageNumber;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        public Boolean getPaged() {
            return paged;
        }

        public void setPaged(Boolean paged) {
            this.paged = paged;
        }

        public Boolean getUnpaged() {
            return unpaged;
        }

        public void setUnpaged(Boolean unpaged) {
            this.unpaged = unpaged;
        }

    }

    public class Sort_ {

        private Boolean sorted;
        private Boolean unsorted;

        public Boolean getSorted() {
            return sorted;
        }

        public void setSorted(Boolean sorted) {
            this.sorted = sorted;
        }

        public Boolean getUnsorted() {
            return unsorted;
        }

        public void setUnsorted(Boolean unsorted) {
            this.unsorted = unsorted;
        }

    }

    public class Seller {

        private Integer vkId;
        private String name;
        private String city;
        private String photoMax;

        public Integer getVkId() {
            return vkId;
        }

        public void setVkId(Integer vkId) {
            this.vkId = vkId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getPhotoMax() {
            return photoMax;
        }

        public void setPhotoMax(String photoMax) {
            this.photoMax = photoMax;
        }

    }

    public class VkAlbum {

        private Integer vkId;
        private String name;
        private String category;
        private String status;
        private Boolean deleted;

        public Integer getVkId() {
            return vkId;
        }

        public void setVkId(Integer vkId) {
            this.vkId = vkId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Boolean getDeleted() {
            return deleted;
        }

        public void setDeleted(Boolean deleted) {
            this.deleted = deleted;
        }


    }

    public class VkGroup {

        private Integer vkId;
        private String name;
        private String status;
        private List<Album> albums = null;
        private Boolean deleted;

        public Integer getVkId() {
            return vkId;
        }

        public void setVkId(Integer vkId) {
            this.vkId = vkId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Album> getAlbums() {
            return albums;
        }

        public void setAlbums(List<Album> albums) {
            this.albums = albums;
        }

        public Boolean getDeleted() {
            return deleted;
        }

        public void setDeleted(Boolean deleted) {
            this.deleted = deleted;
        }

    }

    public class Sort {

        private Boolean sorted;
        private Boolean unsorted;

        public Boolean getSorted() {
            return sorted;
        }

        public void setSorted(Boolean sorted) {
            this.sorted = sorted;
        }

        public Boolean getUnsorted() {
            return unsorted;
        }

        public void setUnsorted(Boolean unsorted) {
            this.unsorted = unsorted;
        }

    }

    public class Album {

        private Integer vkId;
        private String name;
        private String category;
        private String status;
        private Boolean deleted;

        public Integer getVkId() {
            return vkId;
        }

        public void setVkId(Integer vkId) {
            this.vkId = vkId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Boolean getDeleted() {
            return deleted;
        }

        public void setDeleted(Boolean deleted) {
            this.deleted = deleted;
        }

    }
}


