package ru.nikitadrzh.airsoftbulletin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by Nekit on 14.03.2018.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {
    private AirsoftFragment airsoftFragment = (AirsoftFragment) AirsoftActivity.staticFragment;

    @Override
    public void onReceive(Context context, Intent intent) {

        ConnectivityManager manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = manager != null
                ? manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI) : null;
        NetworkInfo mobile = manager != null
                ? manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) : null;

//        if (((wifi != null && wifi.isConnected()) ||
//                (mobile != null && mobile.isConnected()))
//                && airsoftFragment.isReceiverRegistered()) {
//            airsoftFragment.onNetworkChanged();
//        }
    }
}
