package ru.nikitadrzh.airsoftbulletin;

import io.realm.RealmObject;

/**
 * Created by Nekit on 20.02.2018.
 */

public class FavouritesItem extends RealmObject {//модель для RealM
    private String sellerName;
    private String sellerCity;
    private String sellerPhotoUrl;
    private String sellerVkId;

    private String itemPhotoUrl;
    private String itemAddTime;
    private String itemDescription;

    private String marketName;

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getSellerCity() {
        return sellerCity;
    }

    public void setSellerCity(String sellerCity) {
        this.sellerCity = sellerCity;
    }

    public String getSellerPhotoUrl() {
        return sellerPhotoUrl;
    }

    public void setSellerPhotoUrl(String sellerPhotoUrl) {
        this.sellerPhotoUrl = sellerPhotoUrl;
    }

    public String getItemPhotoUrl() {
        return itemPhotoUrl;
    }

    public void setItemPhotoUrl(String itemPhotoUrl) {
        this.itemPhotoUrl = itemPhotoUrl;
    }

    public String getItemAddTime() {
        return itemAddTime;
    }

    public void setItemAddTime(String itemAddTime) {
        this.itemAddTime = itemAddTime;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public String getSellerVkId() {
        return sellerVkId;
    }

    public void setSellerVkId(String sellerVkId) {
        this.sellerVkId = sellerVkId;
    }
}
