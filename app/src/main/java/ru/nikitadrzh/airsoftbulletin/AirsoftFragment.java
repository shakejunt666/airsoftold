package ru.nikitadrzh.airsoftbulletin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Cache;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

import static java.lang.Long.valueOf;

/**
 * Created by Nekit on 08.02.2018.
 */

public class AirsoftFragment extends Fragment {
    private static final String METHOD = "method";
    private static final String CATEGORY = "category";
    private static final String SEARCH = "search";
    private static final String SELLERID = "sellerId";
    private static final String TAG = "AirsoftFragment";

    private int richMediaWigth;//ширина контента cardView
    private int richMediaHeight;
    private int numOfPage = 1;//первая страница
    private int countPerPage = 20;//количество объявлений на странице

    private RecyclerView mRecyclerView;
    private ItemAdapter adapter;
    private ProgressBar progressBar;

    private AdItem adItemList = new AdItem();//список с полученными объявлениями

    private String query = null;
    private String category = null;

    private Realm realm;
    private RealmResults<FavouritesItem> results;
    private RealmThread<ItemHolder> realmThread;

    private LruCache cache;
    private HashMap<String, Target> map;
    private int cachedPhotoCount = 5;

    private NetworkChangeReceiver receiver;

    private boolean isReceiverRegistered;

    //статический метод создания фрагмента
    public static AirsoftFragment newInstance(String method,
                                              String category,
                                              String search,
                                              int seller) {
        //todo 1 создание фрагмента
        Bundle args = new Bundle();

        args.putString(METHOD, method);
        args.putString(CATEGORY, category);
        args.putString(SEARCH, search);
        args.putInt(SELLERID, seller);

        AirsoftFragment airsoftFragment = new AirsoftFragment();
        airsoftFragment.setArguments(args);

        return airsoftFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //todo 2 создание Rec.View, адаптера и пустого списка
        getRichMediaSize();//ширина, высота контента cardView
        cache = ((AirsoftActivity) getActivity()).initPhotoCache();//инициализация cache для
        // предварительной загрузки
        map = new HashMap<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_airsoft, container, false);

        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        setupAdapter(adItemList);//подключаем адаптер с пустым списком при инициализации

        progressBar = view.findViewById(R.id.progress_bar);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //todo 3 инициализация загрузки с сервера
        startDownloading(numOfPage, getArguments().getString(METHOD),
                getArguments().getString(SEARCH), getArguments().getString(CATEGORY),
                getArguments().getInt(SELLERID), countPerPage);
        receiver = new NetworkChangeReceiver();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cache.clear();//очистка кэша
        map.clear();

        if (isReceiverRegistered) {
            getContext().unregisterReceiver(receiver);
            receiver = null;
            Log.i(TAG, "receiver was unregistered from onDestroy");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setupAdapter(List<AdItem> items) {
        adapter = new ItemAdapter(items);
        mRecyclerView.setAdapter(adapter);
    }

    private void startDownloading(int page, String method, String query, String category,
                                  int sellerVkId, int countPerPage) {
        new DownloadAdsTask(this, page, query, category, sellerVkId, countPerPage, method)
                .execute();//если без сортировки
    }

    public ItemAdapter getAdapter() {
        return adapter;
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        private TextView descriptionTextView;
        private TextView dateTextView;
        private TextView sellerNameView;
        private TextView cityNameView;
        private TextView marketNameView;
        private ImageView adItemImageView;
        private ImageView sellerImageView;
        private ImageButton expandButton;

        private ImageButton favouriteButton;

        private String sellerVkId;
        private String sellerPhotoUrl;
        private String itemPhotoUrl;

        private Disposable subscription;

        public ItemHolder(View itemView) {
            super(itemView);
            descriptionTextView = itemView.findViewById(R.id.description_text_view);
            dateTextView = itemView.findViewById(R.id.date_text_view);
            adItemImageView = itemView.findViewById(R.id.ad_item_image_view);
            adItemImageView.setMinimumHeight(richMediaHeight);//установка высоты для места под
            // изображение объявления
            adItemImageView.setOnClickListener(v -> ((AirsoftActivity) getActivity())
                    .getFullAdImage(itemPhotoUrl));

            sellerImageView = itemView.findViewById(R.id.seller_image_view);
            sellerImageView.setOnClickListener(v -> ((AirsoftActivity) getActivity())
                    .startConversation("https://vk.com/im?sel=" + sellerVkId));
            sellerNameView = itemView.findViewById(R.id.seller_name_text_view);
            cityNameView = itemView.findViewById(R.id.city_name_text_view);
            marketNameView = itemView.findViewById(R.id.market_name_text_view);
            expandButton = itemView.findViewById(R.id.expand_button);
            expandButton.setOnClickListener(v -> {//по нажатию кнопки появляется или скрывается
                // описание товара
                if (descriptionTextView.getVisibility() == View.GONE) {
                    descriptionTextView.setVisibility(View.VISIBLE);
                    expandButton.setImageResource(R.drawable.ic_expand_less);
                } else {
                    descriptionTextView.setVisibility(View.GONE);
                    expandButton.setImageResource(R.drawable.ic_expand_more);
                }
            });
            favouriteButton = itemView.findViewById(R.id.button_favourite);
            favouriteButton.setOnClickListener(v -> {//по нажатию добавляется инфа в базу, по
                // повторному удаляется из нее
                realm = Realm.getDefaultInstance();//get Realm object
                try {
                    subscription = realm.where(FavouritesItem.class)
                            .findAllAsync()
                            .asFlowable()
                            .filter(RealmResults::isLoaded)
                            .subscribe(favouritesItems -> toSubscribers());//после загрузки
                    // вызывается метод toSubscribers
                } finally {
                    realm.close();//нужно обязательно закрывать
                }
            });
        }

        private void toSubscribers() {//в этом методе расписываем действия подписчиков
            AdItem adItem = adItemList.get(ItemHolder.this.getAdapterPosition());//определение
            // элемента в RecView
            if (adItem.isFavourite()) {//убираем из базы, если помечено как избранное
                adItem.setFavourite(false);//в списке изменяем поле Fav
                RealmResults<FavouritesItem> results = realm.where(FavouritesItem.class)//ищем в
                        // базе записи с таким url фото
                        .equalTo("itemPhotoUrl", adItem.getPhoto_src())//сравниваем поля
                        // url фотографии с сервера и из базы
                        .findAll();
                realm.executeTransaction(realm1 -> results.deleteAllFromRealm()); //удаление из БД
                setColorStar(ItemHolder.this, adItem.isFavourite());//перекрашиваем звезду

            } else {//добавляем в базу
                adItem.setFavourite(true);
                realm.executeTransaction(realm -> {
                    FavouritesItem item = realm.createObject(FavouritesItem.class); //создание
                    // объекта в базе
                    item.setSellerName(sellerNameView.getText().toString());
                    item.setSellerCity(cityNameView.getText().toString());
                    item.setSellerPhotoUrl(getSellerPhotoUrl());
                    item.setSellerVkId(sellerVkId);

                    item.setItemPhotoUrl(getItemPhotoUrl());
                    item.setItemAddTime(dateTextView.getText().toString());
                    item.setItemDescription(descriptionTextView.getText().toString());
                    item.setMarketName(marketNameView.getText().toString());
                });
                setColorStar(ItemHolder.this, adItem.isFavourite());//перекрашиваем звезду
            }
            subscription.dispose();
        }

        ImageView getAdItemImageView() {
            return adItemImageView;
        }

        ImageView getSellerImageView() {
            return sellerImageView;
        }

        ImageButton getFavouriteButton() {
            return favouriteButton;
        }

        void setFavouriteButton(int resDrawable) {
            getFavouriteButton().setImageResource(resDrawable);
        }

        void setSellerNameView(String text) {
            sellerNameView.setText(text);
        }

        void setCityNameView(String text) {
            cityNameView.setText(text);
        }

        void setMarketNameView(String text) {
            marketNameView.setText(text);
        }

        void setDateTextView(String text) {
            dateTextView.setText(text);
        }

        void setDescriptionTextView(String text) {
            descriptionTextView.setText(text);
        }

        void setVisibilityDescription(int visibilityState) {
            descriptionTextView.setVisibility(visibilityState);
        }

        void setSellerVkId(String Id) {
            sellerVkId = Id;
        }

        String getSellerPhotoUrl() {
            return sellerPhotoUrl;
        }

        void setSellerPhotoUrl(String sellerPhotoUrl) {
            this.sellerPhotoUrl = sellerPhotoUrl;
        }}}

        String getItemPhotoUrl() {
            return itemPhotoUrl;
        }

        void setItemPhotoUrl(String itemPhotoUrl) {
            this.itemPhotoUrl = itemPhotoUrl;
        }
    }

    private class ItemAdapter extends RecyclerView.Adapter<ItemHolder> {
        private int maxCacheSize = 30;//кол-во элементов в кэше максимум
        private int adapterPosition;

        private ItemAdapter(List<AdItem> items) {
            adItemList = items;
        }

        @Override
        public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //todo test
            View view = LayoutInflater.from(getActivity())
                    .inflate(R.layout.ad_item, parent, false);
            return new ItemHolder(view);
        }

        @Override
        public void onBindViewHolder(final ItemHolder holder, int position) {
            //todo test
            adapterPosition = holder.getAdapterPosition();

            if (position == adItemList.size() - 10) {//загрузка новых объявлений начинается заранее
                downloadNewPage();//загрузка новой страницы при достижении определенной поз
            }

            holder.adItemImageView.setImageDrawable(null);

            holder.setDescriptionTextView(adItemList.get(position).getDescription());
            holder.setVisibilityDescription(View.GONE);
            holder.expandButton.setImageResource(R.drawable.ic_expand_more);//нужно для того, чтобы
            // у вновь созданных view правильно отображалось
            holder.setSellerNameView(adItemList.get(position).getSeller().getName());
            holder.setSellerVkId(adItemList.get(position).getSeller().getVkId());

            holder.setCityNameView(adItemList.get(position).getSeller().getCity());
            holder.setMarketNameView(adItemList.get(position).getGroup().getName());
            holder.setDateTextView(ConvertTimestamp(valueOf(adItemList
                    .get(position).getTimestamp())));//метод для перевода timestamp в нормальный вид

            setColorStar(holder, adItemList.get(position).isFavourite());
            holder.setItemPhotoUrl(adItemList.get(position).getPhoto_src());

            setupDownloadToCache(position);//настройка загрузки
            bindItemPhoto(position, holder);//биндим фото в холдер (из кэша или напрямую загружаем)
            clearPhotoCache();//очистка кэша при переполнении

            holder.setSellerPhotoUrl(adItemList.get(position).getSeller().getPhotoMax());
            Picasso.with(getActivity())//добавляется фотография продавца, тут resize не нужен, тк
                    // аватарки ВК всего 200px в диаметре
                    .load(adItemList.get(position).getSeller().getPhotoMax())
                    .into(holder.getSellerImageView());
        }

        @Override
        public void onViewRecycled(ItemHolder holder) {
            super.onViewRecycled(holder);
            Log.i("PicCache", "was recycled: "
                    + holder.getAdapterPosition());
            //когда вью перерабатывается, то отменяем загрузку в кэш, чтоб не спамить при быстрой
            // прокрутке
            Picasso.with(getActivity()).cancelRequest(map.get(holder.getItemPhotoUrl()));
            Log.i("PicCache", "отмена загрузки");
        }

        @Override
        public int getItemCount() {
            if (adItemList == null) {
                return 0;
            } else {
//                return adItemList.size();
            }
        }

        public void downloadNewPage() {//метод загрузки новой страницы с сервера
            numOfPage++;
            startDownloading(numOfPage, getArguments().getString(METHOD),
                    getArguments().getString(SEARCH), getArguments().getString(CATEGORY),
                    getArguments().getInt(SELLERID), countPerPage);
        }

        private void setupDownloadToCache(int position) {//определяем как будет происходить загрузка
            if (position == 0) {//для первой позиции грузим еще несколько фото
                int positionInLoop = position;//чтобы не менять position вне while
                while (positionInLoop < position + cachedPhotoCount) {
                    picassoToCache(positionInLoop);
                    positionInLoop++;
                }
            } else {//для всех остальных позиций
                if (position + cachedPhotoCount < adItemList.size()) {//если список уже загрузился
                    picassoToCache(position + cachedPhotoCount);
                }
            }
        }

        private void picassoToCache(int position) {//метод загрузки в кэш через picasso
            if (cache.get(adItemList.get(position).getPhoto_src()) == null
                    && map.get(adItemList.get(position).getPhoto_src()) == null) {//если еще не
                // кэширован
                Target target = new BitmapTarget(position);
                map.put(adItemList.get(position).getPhoto_src(), target);//чтоб ссылки
                // сохранялись
                Picasso.with(getActivity())
                        .load(adItemList.get(position).getPhoto_src())
                        .resize(richMediaWigth, richMediaHeight)//ширина, высота под cardView
                        .centerCrop()
                        .into(target);
                Log.i("PicCache", String.valueOf(adItemList.size()));
            }
        }

        private void bindItemPhoto(int position, ItemHolder holder) {
            if (cache.get(adItemList.get(position).getPhoto_src()) != null) {//если есть в кэше
                Bitmap bitmap = cache.get(adItemList.get(position).getPhoto_src());
                holder.getAdItemImageView().setImageBitmap(bitmap);
                Log.i("PicCache", "position " + position + " from cache");
            } else {//если нет в кэше
                Picasso.with(getActivity())//добавляется фотография объявления
                        .load(adItemList.get(position).getPhoto_src())
                        .resize(richMediaWigth, richMediaHeight)//ширина, высота под cardView
                        .centerCrop()
                        .into(holder.getAdItemImageView());
                Log.i("PicCache", "position " + position + " from picasso");
            }
        }

        private void clearPhotoCache() {//метод очистки кэша при переполнении
            if (map.size() % maxCacheSize == 0 && map.size() != 0) {//очистка кэша
                cache.clear();
                Log.i("PicCache", "cache was clear " + " cache size is: " + cache.size()
                        + " map size is: " + map.size());
            }
        }
    }

    private void setColorStar(ItemHolder holder, boolean isFavourite) {
        if (isFavourite) {
            holder.setFavouriteButton(R.drawable.ic_star_colored);//меняем на цветную звезду
        } else {
            holder.setFavouriteButton(R.drawable.ic_star_border);//меняем на обычную звезду
        }
    }

    private void getRichMediaSize() {//возвращается размер фотографии объявления
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();//контейнер, содержащий 2 координаты
        display.getSize(size);
        double widthOfScreen = size.x - 16;//16 - учет отступа cardView от края экрана

        richMediaWigth = (int) widthOfScreen;//ширина
        richMediaHeight = (int) (widthOfScreen / 16 * 9);//высота
    }

    public String ConvertTimestamp(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp * 1000);//переводим из сек в миллисекунды, тут смещается
        // по часовым поясам, которые в системе стоят
        return createDateString(calendar);
    }

    public String createDateString(Calendar adCalendar) {
        String finalDate;
        SimpleDateFormat shortFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat fullFormat = new SimpleDateFormat("dd.MM в HH:mm");
        Calendar currentCalendar = Calendar.getInstance();

        if ((currentCalendar.get(Calendar.DAY_OF_YEAR) -
                adCalendar.get(Calendar.DAY_OF_YEAR)) == 0) {
            finalDate = "Cегодня в " + shortFormat.format(adCalendar.getTime());
        } else if ((currentCalendar.get(Calendar.DAY_OF_YEAR) -
                adCalendar.get(Calendar.DAY_OF_YEAR)) == 1) {
            finalDate = "Вчера в " + shortFormat.format(adCalendar.getTime());
        } else if ((currentCalendar.get(Calendar.DAY_OF_YEAR) -
                adCalendar.get(Calendar.DAY_OF_YEAR)) == 2) {
            finalDate = "Позавчера в " + shortFormat.format(adCalendar.getTime());
        } else {
            finalDate = "Добавлено " + fullFormat.format(adCalendar.getTime());
        }
        return finalDate;
    }

    private class BitmapTarget implements Target {
        int position;

        BitmapTarget(int position) {
            this.position = position;
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            cache.set(adItemList.get(position).getPhoto_src(), bitmap);
            Log.i("PicCache", "success download" + position);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    }

    private static class DownloadAdsTask extends AsyncTask<Void, Void, List<AdItem>> {
        private WeakReference<AirsoftFragment> fragmentWeakReference;//ссылка на фрагмент

        private int numOfPage;
        private int sellerVkId;
        private int countPerPage;

        private String method;
        private String queryString;
        private String categoryString;

        DownloadAdsTask(AirsoftFragment context, int page, String query, String category,
                        int sellerId, int pageCount, String queryMethod) {
            fragmentWeakReference = new WeakReference<>(context);//получаем ссылку на фрагмент
            method = queryMethod;
            numOfPage = page;//передается номер страницы
            queryString = query;
            categoryString = category;
            sellerVkId = sellerId;
            countPerPage = pageCount;
            if (fragmentWeakReference.get().adItemList.size() == 0) {
                fragmentWeakReference.get().progressBar.setVisibility(View.VISIBLE);//progress bar
            }
        }

        //вызывается метод загрузки данных
        @Override
        protected AdItem doInBackground(Void... voids) {
            return new AirsoftDownloader().downloadItemList(method, numOfPage, queryString,
                    categoryString, sellerVkId, countPerPage);
        }

        @Override
        protected void onPostExecute(List<AdItem> adItems) {

            //todo test
            AirsoftFragment airsoftFragment = fragmentWeakReference.get();//по ссылке получаем
            if (airsoftFragment.isDetached()) return; //если фрагмент detached

            airsoftFragment.progressBar.setVisibility(View.GONE);//progress bar off
            if (adItems == null) {//если интернета нет, то список пустой и срабатывает return
                //todo 5 исключение IOE выброшено
                Toast.makeText(airsoftFragment.getContext()
                        , "Check internet connection"
                        , Toast.LENGTH_LONG)
                        .show();
                //todo 6 регистрируем receiver
                airsoftFragment.isReceiverRegistered = true;
                airsoftFragment.registerNetworkChangeReceiver();
                Log.i(TAG, "waiting for connection...");
                return;
            }

            //todo 4 исключение IOE не выброшено
            //todo test заполняется ли адаптер, если он пустой изначально
            airsoftFragment.adItemList.addAll(adItems);
            airsoftFragment.mRecyclerView
                    .getAdapter()
                    .notifyItemChanged(airsoftFragment.adItemList.size() - adItems.size());
            Log.i(TAG, "Success downloading! "
                    + "List size is: "
                    + airsoftFragment.adItemList.size());
        }
    }

    private void registerNetworkChangeReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        getContext().registerReceiver(receiver, filter);
    }

    public void onNetworkChanged() {
        //todo 7 отсоединяем receiver
        Log.i("TAG", "connection established!");
        getContext().unregisterReceiver(receiver);//отсоединяем ресивер
        isReceiverRegistered = false;
        Log.i("TAG", "reciever unregistered from onNetworkChanged");

        //todo 8 запускаем инициализацию загрузки с сервера
        if (adapter.adapterPosition >= adItemList.size() - 10
                && adapter.adapterPosition < adItemList.size()) {//для загрузки новой страницы
            adapter.downloadNewPage();
            Log.i(TAG, "was downloaded new page!");
        } else if (numOfPage == 1) {//для загрузки первой страницы
            startDownloading(numOfPage, getArguments().getString(METHOD),
                    getArguments().getString(SEARCH), getArguments().getString(CATEGORY),
                    getArguments().getInt(SELLERID), 20);
            Log.i(TAG, "was downloaded first page!");
        }
        Toast.makeText(getContext(), "Connection established!", Toast.LENGTH_SHORT)
                .show();
    }

    public boolean isReceiverRegistered() {
        return isReceiverRegistered;
    }
}
