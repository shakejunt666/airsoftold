package ru.nikitadrzh.airsoftbulletin;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by Nekit on 08.02.2018.
 */

public abstract class SingleFragmentActivity extends AppCompatActivity {//шаблонный класс для
    // упрощения дальнейшей работы
    private BottomNavigationView navigationView;
    private Fragment fragment;

    protected abstract Fragment createFragment(String fragment);//абстрактный метод, для реализации
    // в любой активити

    @LayoutRes
    protected int getLayoutResId() {//субклассы могут переопределить этот метод под свой макет
        return R.layout.activity_fragment;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());

        FragmentManager manager = getSupportFragmentManager();
        fragment = manager.findFragmentById(R.id.fragment_container);//получаем фрагмент,
        // если он уже был создан
        if (fragment == null) {//используя переопределенный метод создается фрагмент
            fragment = createFragment("ads");//возвращается фрагмент
            manager.beginTransaction().add(R.id.fragment_container, fragment).commit();//в контейнер
            // добавляется созданный фрагмент
        }

//        initRealm();//инициализация DB

        navigationView = findViewById(R.id.bottom_navigation_bar);//определяем
        // bottom nav bar
        navigationView.setOnNavigationItemSelectedListener(item -> {
            FragmentManager fragmentManager = getSupportFragmentManager();

            switch (item.getItemId()) {
                case R.id.navigation_ads:
                    if (fragment.getArguments().get("method") != "getSortedItemsList") {//если
                        // метод sort, то используется фрагмент, переданный из airsoftActivity,
                        // иначе создается фрагмент ads
                        fragment = createFragment("ads");
                    }
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, fragment)
                            .commit();
                    //в контейнер добавляется созданный фрагмент
                    fragment = createFragment("ads");//обратно устанавливаем фрагмент, чтоб все
                    // правильно работало
                    return true;
                case R.id.navigation_search:
                    fragment = createFragment("search");
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, fragment)
                            .commit();
                    //в контейнер добавляется созданный фрагмент
                    return true;
                case R.id.navigation_favorites:
                    fragment = createFragment("favorites");
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, fragment)
                            .commit();
                    return true;
            }
            return false;
        });
    }

//    private void initRealm() {
//
//        Realm.init(this);//init Realm
//        RealmConfiguration config = new RealmConfiguration
//                .Builder()
//                .deleteRealmIfMigrationNeeded()
//                .name("favourites.realm")
//                .build();
//        Realm.setDefaultConfiguration(config);
//    }

    public BottomNavigationView getBottomView() {
        return navigationView;
    }

    public void setFragment(Fragment newFragment) {
        fragment = newFragment;
    }
}
