package ru.nikitadrzh.airsoftbulletin;

import android.support.annotation.NonNull;

import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;


import java.io.IOException;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.cert.CertificateException;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Nekit on 08.02.2018.
 */

public class AirsoftDownloader {//сетевой класс
    private static final String TAG = "Downloader";
    private static final String MAINURL = "https://188.225.57.251/api/";//основной URL
    private AdItem adItemList;

    private interface ServerApi {//интерфейс для Retrofit

        @GET("items")
        Call<AdItem> getAllItemsList(@Query("page") int numOfPage);

        @GET("search")
        Call<List<AdItem>> getSortedItemsList(@Query("query") String query,
                                              @Query("category") String category,
                                              @Query("seller") int sellerVkId,
                                              @Query("page") int numOfPage,
                                              @Query("count") int countPerPage);
    }

    public AdItem downloadItemList(String method, int pageNumber, String query,
                                         String category, int sellerVkId, int countPerPage) {
        //метод, который парсит JSON
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MAINURL)
                .client(getUnsafeOkHttpClient().build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ServerApi serverApi = retrofit.create(ServerApi.class);
        try {
            if (method.equals("getAllItemsList")) {
                adItemList = serverApi.getAllItemsList(pageNumber).execute().body();
            } else if (method.equals("getSortedItemsList")) {
                //Log.i("TAG", "");
//                adItemList = serverApi.getSortedItemsList(query, category, sellerVkId, pageNumber,
//                        countPerPage)
//                        .execute()
//                        .body();
            }
//            compareWithRealm();//ищем схожие элементы в БД
        } catch (IOException e) {
            adItemList = null;//при отсутсвии интернета возвращаем пустой список
        }
        return adItemList;
    }

    public static OkHttpClient.Builder getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier((hostname, session) -> true);
            return builder;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

//    private void compareWithRealm() {//метод сравнения полученных данных с сервера и из БД
//        //TODO мб вынести этот метод из сетевого класса?
//        Realm realm = Realm.getDefaultInstance();
//        try {
//            for (AdItem item : adItemList) {
//                RealmResults<FavouritesItem> results = realm.where(FavouritesItem.class)//ищем в
//                        // базе записи с таким url фото
//                        .equalTo("itemPhotoUrl", item.getPhoto_src())//сравниваем поля url
//                        // фотографии с сервера и из базы
//                        .findAll();
//                if (results.size() > 0) item.setFavourite(true);//если url совпадают, то в поле
//                //isFavourites списка записывается true
//                Log.i(TAG, "result is " + results.toString());
//                Log.i(TAG, "check list " + item.isFavourite());//проверка записи в List
//            }
//        } finally {
//            realm.close();
//        }
//    }
}
